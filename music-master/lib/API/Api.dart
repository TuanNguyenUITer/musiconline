import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:loadplaylist/PlayList/Baihat.dart';

class Api {
  // thay đổi string ứng với link play list
  Future<void> getListBaiHat(String url,
      {Function(List<BaiHat>) onSuccess, Function(String) onError}) async {
    List<BaiHat> listbaihat = List();
    http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      try {
        final jsondecode = json.decode(response.body);
        final data = jsondecode['data']['song']['items'];
        data.forEach((p) {
          listbaihat.add(BaiHat.fromJson(p));
        });
        onSuccess(listbaihat);
      } catch (e) {
        onError("Something get wrong!");
      }
    } else {
      onError("Something get wrong! Status code ${response.statusCode}");
    }
  }

  //link lời bài hát bài hát vô đây  trả về list

  //tham sô value tm kiem là nôi dung lấy ở text box search
  Future<void> searchBaiHat(String valueTimKiem,
      {Function(List<BaiHat>) onSuccess, Function(String) onError}) async {
    String endpoint = 'https://zingmp3.vn/api/search?type=song&q=' +
        valueTimKiem +
        '&start=0&count=20&ctime=1575083405&sig=4e2f8c458e8fe8516223757a0234ff84e6ea1381bfa7e242b69d3506b71b9d2becce29fec1ca25370fc6b3e1d2958f8c95bd8da5ac96951b73121105e0afbfea&api_key=38e8643fb0dc04e8d65b99994d3dafff';
    List<BaiHat> listbaihat = List();
    http.Response response = await http.get(endpoint);
    if (response.statusCode == 200) {
      try {
        final jsondecode = json.decode(response.body);
        final data = jsondecode['data']['items'];
        data.forEach((p) {
          listbaihat.add(BaiHat.fromJson(p));
        });
        onSuccess(listbaihat);
      } catch (e) {
        onError("Something get wrong!");
      }
    } else {
      onError("Something get wrong! Status code ${response.statusCode}");
    }
  }

  //link lời bài hát baihat.linklyrric vô đây  trả về list string
  Future<void> LoiBaiHat(String linklyric,
      {Function(String) onSuccess, Function(String) onError}) async {
    var lyric = "";
    http.Response response = await http.get(linklyric);

    if (response.statusCode == 200) {
      try {
        //final jsondecode =
        final data = utf8.decode(response.bodyBytes);
        // print(data);
        lyric = xuLiLyric(data);
        //print(lyric);
        onSuccess(lyric);
      } catch (e) {
        onError("Something get wrong!");
      }
    } else {
      onError("Something get wrong! Status code ${response.statusCode}");
    }
  }
}

xuLiLyric(String input) {
  //console.log(this.state.stringLyric+"uuu")
  var value = '';
  value = input + "[";
  var start = 0;
  var end = 0;
  var data = " ";

  //data['00:00']='haha'
  for (var i = 0; i < value.length; i++) {
    if (value[i] == "]") {
      start = i;
      for (var j = i; j < value.length; j++) {
        if (value[j] == "[") {
          end = j;

          break;
        }
      }
      var line = value.substring(start + 1, end);
      // print(line);
      data += line;
      i = end;
    }
  }
  return data;
}
