import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';
//import 'package:loadplaylist/PlayList/PlayLists.dart';
import 'package:loadplaylist/PlayList/Playlist.dart';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:splashscreen/splashscreen.dart';

import 'package:loadplaylist/Views/InformationPage.dart';
import 'package:connectivity/connectivity.dart';
import 'API/Api.dart';
import 'Views/LibraryPage.dart';
import 'Views/PlayListMusic.dart';
import 'Views/HomePage.dart';
import 'Views/SignInUp.dart';

Future<BaiHat> fetchAlbum(String linkPlaylist) async {
  final response = await http.get(linkPlaylist);
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    final jsondecode = json.decode(response.body);
    final data = jsondecode['data']['song']['items'];
    playlist.listbh = new List();

    for (Map i in data) {
      playlist.listbh.add(BaiHat.fromJson(i));
    }
    return BaiHat.fromJson(data[0]);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(
        loaderColor: Colors.amber,
        seconds: 1,
        backgroundColor: Color(0xFF0A3068),
        navigateAfterSeconds: new MyHomePage(0),
        image: Image.asset("assets/images/bgR.png"),
        photoSize: 150,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final int pageIndex;
  MyHomePage(int this.pageIndex);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  /*int _pageIndex = 0;
  final LibraryPages _libraryPage = new LibraryPages();
  final HomePages _homePage = new HomePages();
  final PlayListMusics _play = new PlayListMusics(
      'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU6ZEAZD&ctime=1576114795&sig=7ce9f0824834903f86b6d21ad0edb8d04459cf361f1f0f73f3551787ca22e308e771839877cd122bbf1a59ee32bf4c079f1950953d173a8c608333b5083803dd&api_key=38e8643fb0dc04e8d65b99994d3dafff');
  final InfomationPages _accountPage = new InfomationPages();
  GlobalKey _bottomNavigationKey = GlobalKey();
  Widget _showPage;
  void initState() {
    super.initState();
    _showPage = _pagechoose(this.widget.pageIndex);
    _pageIndex = this.widget.pageIndex;
  }

  Widget _pagechoose(int page) {
    switch (page) {
      case 0:
        return _homePage;
        break;
      case 2:
        return _play;
        break;
      case 1:
        return _libraryPage;
        break;
      case 3:
        return _accountPage;
        break;
      default:
        return Container(
          child: Text("Error"),
        );
    }
  }     */
  bool result = true;
  bool isLoading = false;

  void checkInternet() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        ChangeValues(true);
      } else {
        ChangeValues(false);
      }
    });
  }

  void ChangeValues(bool resultval) {
    setState(() {
      result = resultval;
    });
  }

  PageController _pageController;
  int _page = 0;
  bool heightMenu = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        onPageChanged: onPageChanged,
        children: <Widget>[
          result ? HomePages() : NoInternet(context),
          result
              ? new PlayListMusics(
                  'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU6ZEAZD&ctime=1576114795&sig=7ce9f0824834903f86b6d21ad0edb8d04459cf361f1f0f73f3551787ca22e308e771839877cd122bbf1a59ee32bf4c079f1950953d173a8c608333b5083803dd&api_key=38e8643fb0dc04e8d65b99994d3dafff',
                  'List nhạc bạn vừa nghe')
              : NoInternet(context),
          result ? HomePages() : NoInternet(context),
          SignInUpPage(false),
          result ? LibraryPages() : NoInternet(context),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: AnimatedContainer(
          height: heightMenu ? 0 : 50,
          color: Color(0xffE5F1FD),
          duration: Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(width: 7),
              IconButton(
                icon: Icon(
                  Icons.home,
                  size: 24.0,
                ),
                color: _page == 0 ? Color(0xFF0A3068) : Colors.grey,
                onPressed: () => _pageController.jumpToPage(0),
              ),
              IconButton(
                icon: Icon(
                  Icons.music_note,
                  size: 24.0,
                ),
                color: _page == 1 ? Color(0xFF0A3068) : Colors.grey,
                onPressed: () => _pageController.jumpToPage(1),
              ),
              IconButton(
                icon: Icon(
                  Icons.add,
                  color: Color(0xffE5F1FD),
                  size: 24.0,
                ),
                color: _page == 2 ? Color(0xFF0A3068) : Colors.grey,
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(
                  Icons.account_circle,
                  size: 24.0,
                ),
                color: _page == 3 ? Color(0xFF0A3068) : Colors.grey,
                onPressed: () => _pageController.jumpToPage(3),
              ),
              IconButton(
                icon: Icon(
                  Icons.history,
                  size: 24.0,
                ),
                color: _page == 4 ? Color(0xFF0A3068) : Colors.grey,
                onPressed: () => _pageController.jumpToPage(4),
              ),
              SizedBox(width: 7),
            ],
          ),
        ),
        color: Colors.grey.withOpacity(.5),
        shape: CircularNotchedRectangle(),
      ),
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFF0A3068),
        elevation: 10.0,
        child: IconButton(
          icon: Icon(
            Icons.music_note,
            color: Colors.white,
          ),
          color: Color(0xFF0A3068),
        ),
        onPressed: () {
          setState(() {
            heightMenu = !heightMenu;
          });
        },
      ),
    );
  }

  void navigationTapped(int page) {
    _pageController.jumpToPage(page);
  }

  @override
  void initState() {
    super.initState();

    _pageController = PageController();
    checkInternet();
    // đây là hàm gọi API muốn load api nào thì sài WidgetsBinding này với  setState
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() => isLoading = true);
      await Api().getListBaiHat(
          'https://zingmp3.vn/api/playlist/get-playlist-detail?id=Z6CZOIWU&ctime=1575943109&sig=c1e46732088a2bb842dfb2d5134f3221afc5fee1a270c3e7f638a95d5118ccbac222639845d564623902bf92e1f8a86c8220bd6d9dd2e980ec92d6a52c3dfae0&api_key=38e8643fb0dc04e8d65b99994d3dafff',
          onSuccess: (values) {
        setState(() {
          isLoading = false;
          playlist.listbh = values;
        });
      }, onError: (msg) {
        setState(() => isLoading = false);
        print(msg);
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  Widget NoInternet(BuildContext context) {
    return Container(
      color: Color(0xFF0A3068),
      height: MediaQuery.of(context).size.height,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("No Interneting"),
            SizedBox(
              height: 10,
            ),
            CircularProgressIndicator(
              strokeWidth: 3,
              backgroundColor: Color(0xFF0A3068),
            ),
          ],
        ),
      ),
    );
  }
}
