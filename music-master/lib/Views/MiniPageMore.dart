import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';

Widget MiniPageMore(
    BuildContext context, BaiHat baiHat, Function fc(), BaiHat bh) {
  showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext more) {
        return Container(
          margin: const EdgeInsets.only(
              top: 10.0, right: 10, left: 10, bottom: 200),
          height: MediaQuery.of(context).size.height * 2 / 5,
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: Color(0xff97A4B7),
                    offset: new Offset(8.0, 10.0),
                    blurRadius: 25.0)
              ],
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff97A4B7),
                                offset: new Offset(8.0, 10.0),
                                blurRadius: 10.0)
                          ],
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: Color(0xFF0A3068).withOpacity(.5),
                              width: 2.5),
                          image: DecorationImage(
                              image: NetworkImage(baiHat.hinhanh),
                              fit: BoxFit.cover)),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 15,
                        ),
                        Expanded(
                            child: Text(
                          baiHat.ten,
                          style: TextStyle(
                              fontSize: 11, fontWeight: FontWeight.bold),
                        )),
                        Expanded(
                            child: Text(
                          baiHat.casi,
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 8,
                              fontWeight: FontWeight.bold),
                        ))
                      ],
                    )),
                    Icon(Icons.music_note)
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                        child: Container(
                      height: 32.0,
                      width: 32.0,
                      decoration: BoxDecoration(
                          color: Color(0xffE5F1FD),
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff97A4B7),
                                offset: new Offset(8.0, 10.0),
                                blurRadius: 25.0)
                          ]),
                      child: IconButton(
                          icon: Icon(
                            Icons.favorite_border,
                            size: 17,
                          ),
                          onPressed: () {
                            fc();
                          }),
                    )),
                    Expanded(
                        child: Container(
                      height: 32.0,
                      width: 32.0,
                      decoration: BoxDecoration(
                          color: Color(0xffE5F1FD),
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff97A4B7),
                                offset: new Offset(8.0, 10.0),
                                blurRadius: 25.0)
                          ]),
                      child: IconButton(
                          icon: Icon(
                            Icons.save_alt,
                            size: 17,
                          ),
                          onPressed: () {}),
                    )),
                    Expanded(
                        child: Container(
                      height: 32.0,
                      width: 32.0,
                      decoration: BoxDecoration(
                          color: Color(0xffE5F1FD),
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff97A4B7),
                                offset: new Offset(8.0, 10.0),
                                blurRadius: 25.0)
                          ]),
                      child: IconButton(
                          icon: Icon(
                            Icons.share,
                            size: 17,
                          ),
                          onPressed: () async {
                            change_alias(str) {
                              str = str.replaceAll(
                                  new RegExp(
                                      r'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ'),
                                  'a');
                              str = str.replaceAll(
                                  new RegExp(r'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ'), 'e');
                              str =
                                  str.replaceAll(new RegExp(r'ì|í|ị|ỉ|ĩ'), 'i');
                              str = str.replaceAll(
                                  new RegExp(
                                      r'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ'),
                                  'o');
                              str = str.replaceAll(
                                  new RegExp(r'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ'), 'u');
                              str =
                                  str.replaceAll(new RegExp(r'ỳ|ý|ỵ|ỷ|ỹ'), 'y');
                              str = str.replaceAll(new RegExp(r'đ'), 'd');
                              str = str.replaceAll(
                                  new RegExp(
                                      r'À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ'),
                                  'A');
                              str = str.replaceAll(
                                  new RegExp(r'È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ'), 'E');
                              str =
                                  str.replaceAll(new RegExp(r'Ì|Í|Ị|Ỉ|Ĩ'), 'I');
                              str = str.replaceAll(
                                  new RegExp(
                                      r'Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ'),
                                  'O');
                              str = str.replaceAll(
                                  new RegExp(r'Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ'), 'U');
                              str =
                                  str.replaceAll(new RegExp(r'Ỳ|Ý|Ỵ|Ỷ|Ỹ'), 'Y');
                              str = str.replaceAll(new RegExp(r'Đ'), 'D');
                              str = str.replaceAll(new RegExp(r','), '');
                              str = str.replaceAll(new RegExp(r' '), '-');
                              return str;
                            }

                            var response = await FlutterShareMe().shareToSystem(
                                msg: 'https://zingmp3.vn/bai-hat/' +
                                    change_alias(bh.ten) +
                                    '-' +
                                    change_alias(bh.casi) +
                                    '/' +
                                    bh.id +
                                    '.html');
                          }),
                    )),
                  ],
                ),
              )
            ],
          ),
        );
      });
}
