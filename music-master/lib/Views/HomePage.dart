import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';
import 'package:loadplaylist/PlayList/ListBaiHat.dart';

//import 'package:loadplaylist/PlayList/PlayLists.dart';
import 'package:loadplaylist/PlayList/Playlist.dart';
import 'package:loadplaylist/Views/Player.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:loadplaylist/Views/PlayListMusic.dart';
import 'dart:math';
import 'package:loadplaylist/PlayList/LibraryPlayList.dart';
import 'package:loadplaylist/PlayList/ListBaiHat.dart';
import 'MiniPageMore.dart';
import 'package:loadplaylist/Views/SearchPages.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePages(),
    );
  }
}

final List<List<String>> ListSlider = [
  [
    'Nhạc Việt được nghe nhiều nhất',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU6BW90D&ctime=1576114897&sig=0c8fef1d9b1a62ea7664e166af6f09977bfc9eda024c52c018b3289896b85d57e8eb4d869661fead5ae07aa2627b6ddf74d293741dad7ffe693d999ad5b6e012&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/5/c/1/b/5c1b63068c4d7b09eb3566fd2c83cd2b.jpg'
  ],
  [
    'Giọng hát mới nổi bật',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZOUWWOOO&ctime=1576114944&sig=50f5f9c9fc91b46e0b92fb0e32518f38f96155b74ad11aa331d739d381f8a9b58e44c4d5576d5a2f17179a9c0cc91f39ecbd19e7c33bf5defaa624fc640d1ace&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/f/b/3/f/fb3f2b70b331332912756e7067ee6f7c.jpg'
  ],
  [
    'Nhạc Việt đầy hứa hẹn',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZWZCOBEF&ctime=1576115001&sig=87a85341a47e787a2afd9613bc27026d315e09688925395f65bedab6329e4a4b2f6116ac11e5ea175cb8158fd3850882210087afc421f1c709c2d94e33d67826&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/5/6/3/1/56311a256f7d465e194af5c5f59d2dc7.jpg'
  ],
  [
    'Dance Việt',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU6ZEAZD&ctime=1576114795&sig=7ce9f0824834903f86b6d21ad0edb8d04459cf361f1f0f73f3551787ca22e308e771839877cd122bbf1a59ee32bf4c079f1950953d173a8c608333b5083803dd&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-playlist-zmp3.zadn.vn/s2/user-playlist?src=HavwqN7EvKCI1oYSFOdq0r1DFvniYQK30GTkXsZPzWHK0c25FelyK5HHOO9kYgu4KGSqWZlOg0LCMNRIDvxw3WSRFufyohDPIbTucppP-KXA0dYRF8or6mPKUPyyYEKS0r0kb6k9vHO6Kdx5PussJGe78SfXmkKANmjdc6g9fqC0G3FHEiJh1140V9iknE444G1sqdkDwL8JLtA0UOplKn1V98PnZEWE6mHmYo6EvbaR1cQ3Cehg7qaDBS5xrA5B05nWZJp5uGXA5c28FClX3LqGUOLXo-TJ2qKgsp6PjmrQ4tlOCfpW2bz6VjjZdR55K0Purtq&size=thumb/240_240'
  ]
];

final List<List<String>> Top100 = [
  [
    'Top100 n'
        'hạc cách mạng',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZWZB96AO&ctime=1575944497&sig=871b94924c386510797085799869a310f8aa8a6f1b7d87677edea2048ec5a6670b85436ac36dc5f1501479d9db6ea0a87a1db9eba2266a90c2f631f9c08fb388&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/f/2/6/4/f26467e87075a96bf974a8c49450bad5.jpg'
  ],
  [
    'Top100 nhạc trữ tình',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZWZB969F&ctime=1575940775&sig=1f73c85f785a5fea6d5b8f180ced99330f71ae7df6b9db2594c3a4e786fdecfc741e1087553713fc427d32333089fc4c3022a3d8fe6c3d3a3fbbc4660dd829ff&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/1/1/7/4/1174de60bc4d99d042f1f49ba276fc0f.jpg'
  ],
  [
    'Top 100 nhạc EDM',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=Z6CZOIWU&ctime=1575943109&sig=c1e46732088a2bb842dfb2d5134f3221afc5fee1a270c3e7f638a95d5118ccbac222639845d564623902bf92e1f8a86c8220bd6d9dd2e980ec92d6a52c3dfae0&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/3/1/8/f/318f209af4a9010695d12747f2c1d62a.jpg'
  ],
  [
    'Top 100 nhac trẻ',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZWZB969E&ctime=1575126606&sig=1ed0e13e940af18fa93bb91b4335bc35c24713571a1da9f53a78211b5710b26fb03aedbb96dffb4f187da49ab882c03db2948c046c62688fd832d1ad88050bb8&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/6/9/9/c/699c2a7279ee84da91ebab1d98407e2c.jpg'
  ]
];

final List<List<String>> ListGoiY = [
  [
    'Nhạc việt nổi bật',
    ' https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU9EUF68&ctime=1576114288&sig=e40aeec45bf9d262efc23153a4f290db09967fc263dcded9395bb67bcc73fec5e6b88a03fda8a9dd5e9500f6d03dca34365d7d2956c4a5b913df9c8cfd96e07b&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/5/2/3/6/52364bc8f5e08e3980fc8c46418c1523.jpg'
  ],
  [
    'K-Pop',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU9EUF76&ctime=1576114380&sig=7b239520eb48c113b2b21aa8708c5f9bbd029303f194fe1ec31ae535d6090258b6fb12a79419b93da4c9d7072b8c7419f875604a19eb7b519c342c02ce43838e&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/7/a/5/1/7a514a7133c75cd4aff4d33d92c9b3d2.jpg'
  ],
  [
    'US - UK',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU9EUF76&ctime=1576114380&sig=7b239520eb48c113b2b21aa8708c5f9bbd029303f194fe1ec31ae535d6090258b6fb12a79419b93da4c9d7072b8c7419f875604a19eb7b519c342c02ce43838e&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/7/d/5/3/7d53d5a113441f171c6cadefb03b7525.jpg'
  ],
  [
    'C-Pop',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU9DCU07&ctime=1576114595&sig=bfc888e876c810fbc238df2091063bd1cbde54a56afe6ebf706cc5691340e2ede19bdfa504f60f71a812b179c787c7fd0a0280a7989c4c9661b61b59d6356c4c&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/1/9/3/f/193ffce10f889d0980340bc8576e15d5.jpg'
  ],
  [
    'Rap việt',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU8IICE0&ctime=1576114639&sig=809b51bcadcccdb167a08966056b68f4eea05fcaea4011aad70243eba42fea476a473f5bcc19c31f86240048e33b42e433cf22772742a7b846ae0a2fd6230e8f&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/7/1/3/c/713cbcc04cc5090ab2c768f71cfc0694.jpg'
  ],
  [
    'Pop balad',
    'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZU8ZUWC9&ctime=1576114740&sig=28af191e8e651aa6c122b6aec7aaf86aacb1a41b948141c9faea318091a6f4c479bf9962d9b0ead2c12257c76df64680b3c30fcde217960ae6b5b6c227c98b9e&api_key=38e8643fb0dc04e8d65b99994d3dafff',
    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/b/d/6/3/bd6352d916d8e714bf2b75c3305d1a97.jpg'
  ]
];

class HomePages extends StatefulWidget {
  @override
  _HomePagesState createState() => _HomePagesState();
}

class _HomePagesState extends State<HomePages> {
  bool _loading = true;
  void loading() async {
    setState(() {
      _loading = true;
    });
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      _loading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    loading();
  }

  TextEditingController textsearch = new TextEditingController();
  Random random = new Random();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF0A3068),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 10, left: 10, right: 10),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height / 30,
                ),
// thanh tim kiem
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 15,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xffE5F1FD),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: TextField(
                      controller: textsearch,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: IconButton(
                              icon: Icon(
                                Icons.search,
                                color: Color(0xFF0A3068),
                              ),
                              onPressed: () {
                                print(textsearch.text);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          Search(textsearch.text),
                                    ));
                              }),
                          hintText: "Search",
                          hintStyle: TextStyle(
                              color: Color(0xFF0A3068), fontSize: 15)),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 45,
                ),
                //load carousel slider
                Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height / 4,
                    child: HomeSlider(context)),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 20,
                ),
//load goiycho ban
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: Text(
                      "Gợi ý cho bạn",
                      style: TextStyle(
                        color: Colors.white,
                        letterSpacing: 2,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 45,
                ),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 4,
                  child: ListMusic(context, ListGoiY),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 20,
                ),

//load top100
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: Text(
                      "Top 100 ",
                      style: TextStyle(
                        color: Colors.white,
                        letterSpacing: 2,
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 45,
                ),

                Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height / 4,
                    child: ListMusic(context, Top100)),
// load listmusic homepage
                SizedBox(
                  height: MediaQuery.of(context).size.height / 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Text(
                        "Có thể bạn muốn nghe ? ",
                        style: TextStyle(
                          color: Colors.white,
                          letterSpacing: 2,
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    Container(
                      height: 45,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/disk.png"),
                            fit: BoxFit.cover),
                        //color: Color(0xffE5F1FD),

                        boxShadow: [
                          BoxShadow(
                              color: Color(0xff97A4B7),
                              offset: new Offset(8.0, 10.0),
                              blurRadius: 25.0)
                        ],
                      ),
                      child: IconButton(
                        key: Key('play_button'),
                        onPressed: () {
                          setState(() {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PlayerWidget(
                                      baihat:
                                          playlist.listbh[random.nextInt(10)]),
                                ));
                          });
                        },
                        iconSize: 22.0,
                        icon: Icon(Icons.play_arrow),
                        color: Color(0xFF0A3068),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 45,
                ),
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).size.height / 4.5,
                  padding: const EdgeInsets.only(
                      top: 15.0, bottom: 15, left: 5, right: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xff97A4B7),
                          offset: new Offset(8.0, 10.0),
                          blurRadius: 25.0)
                    ],
                  ),
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PlayerWidget(
                                      baihat: playlist.listbh[index]),
                                ));
                          });
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height / 10,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
// số thứ tywj trong listmusic
                                    Text(
                                      (index + 1).toString(),
                                      style: TextStyle(
                                          letterSpacing: 2,
                                          fontSize: 10,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF0A3068)),
                                    ),
                                    //hinh anh trong listmusci
                                    Container(
                                      width:
                                          MediaQuery.of(context).size.height /
                                                  10 -
                                              30,
                                      height:
                                          MediaQuery.of(context).size.height /
                                                  10 -
                                              30,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(
                                            color: Color(0xFF0A3068), width: 2),
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: NetworkImage(playlist
                                                .listbh[index].hinhanhlon)),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    //tên bài hát và tên ca si
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Flexible(
                                          child: Text(
                                              playlist.listbh[index].ten,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  letterSpacing: 1.2,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xFF0A3068))),
                                        ),
                                        Flexible(
                                          child: Text(
                                              playlist.listbh[index].casi,
                                              style: TextStyle(
                                                  letterSpacing: 1.2,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.grey)),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            //button more
                            Align(
                              alignment: Alignment.centerRight,
                              child: IconButton(
                                icon: Icon(Icons.more_vert,
                                    color: Color(0xFF0A3068)),
                                onPressed: () {
                                  MiniPageMore(context, playlist.listbh[index],
                                      () {
                                    setState(() {
                                      BaiHat bh1 = new BaiHat(
                                          playlist.listbh[index].id,
                                          playlist.listbh[index].ten,
                                          playlist.listbh[index].casi,
                                          playlist.listbh[index].hinhanh,
                                          playlist.listbh[index].hinhanhlon,
                                          playlist.listbh[index].linkLyric,
                                          playlist.listbh[index].duration
                                              .toString());
                                      int check = 0;
                                      for (int i = 0;
                                          i < SongInPlayList.length;
                                          i++) {
                                        if (bh1.id == SongInPlayList[i].id)
                                          check++;
                                      }
                                      if (check == 0) {
                                        SongInPlayList.add(bh1);
                                      }

                                      Navigator.pop(context);
                                    });
                                  }, playlist.listbh[index]);
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    itemCount: playlist.listbh.length - 2,
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Widget HomeSlider(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return ItemSlider(context, index, 0);
      },
      itemCount: ListSlider.length,
    );
  }

  //list goi y cho ban
  Widget ListMusic(BuildContext context, List<List<String>> playlist) {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                //
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PlayListMusics(
                            playlist[index][1], playlist[index][0])));
              });
            },
            child: Itemlist(context, index, 0, playlist),
          );
        },
        itemCount: playlist.length //playlist.length,
        );
  }

  // item carouselslider home
  Widget ItemSlider(BuildContext mediacontect, int i, int j) {
    return AspectRatio(
      aspectRatio: MediaQuery.of(context).size.width /
          MediaQuery.of(context).size.width *
          1.9,
      child: Container(
        margin: EdgeInsets.only(right: 8, left: 8),
        decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(ListSlider[i][2]), fit: BoxFit.fitHeight),
          color: Colors.amber,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/disk.png"),
                        fit: BoxFit.cover),
                    //color: Color(0xffE5F1FD),

                    boxShadow: [
                      BoxShadow(
                          color: Color(0xff97A4B7),
                          offset: new Offset(8.0, 10.0),
                          blurRadius: 25.0)
                    ],
                  ),
                  child: IconButton(
                    key: Key('play_button'),
                    onPressed: () {
                      setState(() {
                        LibraryPlayList bh1 = new LibraryPlayList(
                            1,
                            ListSlider[i][0],
                            ListSlider[i][2],
                            0,
                            ListSlider[i][1]);
                        int check = 0;
                        for (int i = 0; i < LibraryPL.length; i++) {
                          if (bh1.GetName == LibraryPL[i].GetName) check++;
                        }
                        if (check == 0) {
                          LibraryPL.add(bh1);
                        }
                        print(LibraryPL.length);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PlayListMusics(
                                  ListSlider[i][1], ListSlider[i][0]),
                            ));
                      });
                    },
                    iconSize: 22.0,
                    icon: Icon(Icons.play_arrow),
                    color: Color(0xFF0A3068),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Flexible(
                  child: Text(
                    ListSlider[i][0],
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.white,
                      letterSpacing: 2,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //item list goi y cho ban
  Widget Itemlist(
      BuildContext mediacontect, int i, int j, List<List<String>> playlist) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: Container(
        margin: EdgeInsets.only(right: 8, left: 8),
        decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(playlist[i][2]), fit: BoxFit.cover),
          color: Colors.amber,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Align(
            alignment: Alignment.bottomLeft,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/disk.png"),
                        fit: BoxFit.fill),
                    //color: Color(0xffE5F1FD),

                    boxShadow: [
                      BoxShadow(
                          color: Color(0xff97A4B7),
                          offset: new Offset(8.0, 10.0),
                          blurRadius: 25.0)
                    ],
                  ),
                  child: IconButton(
                    key: Key('play_button'),
                    onPressed: () {
                      setState(() {
                        LibraryPlayList bh1 = new LibraryPlayList(1,
                            playlist[i][0], playlist[i][2], 0, playlist[i][1]);
                        int check = 0;
                        for (int i = 0; i < LibraryPL.length; i++) {
                          if (bh1.GetName == LibraryPL[i].GetName) check++;
                        }
                        if (check == 0) {
                          LibraryPL.add(bh1);
                        }
                        print(LibraryPL.length);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PlayListMusics(
                                  playlist[i][1], playlist[i][0]),
                            ));
                      });
                    },
                    iconSize: 12.0,
                    icon: Icon(Icons.play_arrow),
                    color: Color(0xFF0A3068),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Flexible(
                  child: Text(
                    playlist[i][0],
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.white,
                      letterSpacing: 2,
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/*MaterialPageRoute(
                    builder: (context) =>
                        PlayerWidget(baihat: playlist.listbh[index]),
                  )*/
