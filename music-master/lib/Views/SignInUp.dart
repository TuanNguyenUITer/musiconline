import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:loadplaylist/PlayList/ClassInfo.dart';
import 'package:loadplaylist/PlayList/ListBaiHat.dart';
import 'package:loadplaylist/Views/InformationPage.dart';

class SignInUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SignInUpPage(false),
    );
  }
}

class SignInUpPage extends StatefulWidget {
  bool check;
  SignInUpPage(this.check);
  @override
  _SignInUpPageState createState() => _SignInUpPageState();
}

class _SignInUpPageState extends State<SignInUpPage> {
  TextEditingController _username = new TextEditingController();
  TextEditingController _Pass = new TextEditingController();
  TextEditingController _newusername = new TextEditingController();
  TextEditingController _newpass = new TextEditingController();
  TextEditingController _confirmpass = new TextEditingController();
  bool eye = true;
  bool error = false;
  String errors = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widget.check
          ? InfomationPages(_newusername.text, _newpass.text, "Người dùng mới",
              "Infos[i].urlimage")
          : SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: PageView(
                  children: [
                    // ui from đăng nhập
                    Container(
                      decoration: BoxDecoration(color: Color(0xFF0A3068)),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                          children: [
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 10),
                            Container(
                              height: MediaQuery.of(context).size.height / 10,
                              width: MediaQuery.of(context).size.width / 4,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                          AssetImage("assets/images/bgR.png"),
                                      fit: BoxFit.fill)),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 20),
                            Text(
                              "Sign In",
                              style: TextStyle(
                                  letterSpacing: 2,
                                  fontSize: 27,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 10),
                            SocialBoxSignInUpName(
                                context, "UserName", _username),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 20),
                            SocialBoxSignInUpPass(context, "Password", _Pass),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 20),
                            Container(
                              height: MediaQuery.of(context).size.height / 15,
                              width: MediaQuery.of(context).size.width / 2,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.white.withOpacity(.3),
                                      width: 2),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xff97A4B7),
                                        offset: new Offset(8.0, 10.0),
                                        blurRadius: 25.0)
                                  ],
                                  color: Color(0xFF0A3068),
                                  borderRadius: BorderRadius.circular(15)),
                              child: FlatButton(
                                  onPressed: () {
                                    for (int i = 0; i < Infos.length; i++) {
                                      if (Infos[i].username == _username.text &&
                                          Infos[i].password == _Pass.text) {
                                        setState(() {
                                          error = false;
                                        });
                                        setState(() {
                                          widget.check = true;
                                        });
                                      } else
                                        setState(() {
                                          widget.check = false;
                                          error = true;
                                        });
                                    }
                                  },
                                  child: Text(
                                    "Đăng nhập",
                                    style: TextStyle(
                                        letterSpacing: 2,
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  )),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 30),
                            Text(
                              "Quên mật khẩu?",
                              style: TextStyle(
                                  letterSpacing: 2,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.grey),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 80),
                            Container(
                              height: 1,
                              width: 100,
                              color: Colors.grey,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 100),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  "Đăng Ký?",
                                  style: TextStyle(
                                      letterSpacing: 2,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                                Text(
                                  " Vuốt sang phải -> ",
                                  style: TextStyle(
                                      letterSpacing: 2,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.grey),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),

//ui from đăng ký
                    Container(
                      decoration: BoxDecoration(color: Color(0xFF0A3068)),
                      child: Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            children: [
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 10),
                              Text(
                                "Sign Up",
                                style: TextStyle(
                                    letterSpacing: 2,
                                    fontSize: 27,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 10),
                              SocialBoxSignInUpName(
                                  context, "UserName", _newusername),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 20),
                              SocialBoxSignInUpPass(
                                  context, "Password", _newpass),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 20),
                              SocialBoxSignInUpPass(
                                  context, "Nhập lại Password", _confirmpass),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 20),
                              Container(
                                height: MediaQuery.of(context).size.height / 15,
                                width: MediaQuery.of(context).size.width / 2,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.white.withOpacity(.3),
                                        width: 2),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0xff97A4B7),
                                          offset: new Offset(8.0, 10.0),
                                          blurRadius: 25.0)
                                    ],
                                    color: Color(0xFF0A3068),
                                    borderRadius: BorderRadius.circular(15)),
                                child: FlatButton(
                                    onPressed: () {
                                      if (_newusername.text.isEmpty ||
                                          _newpass.text.isEmpty ||
                                          _confirmpass.text.isEmpty)
                                        setState(() {
                                          widget.check = false;
                                        });
                                      else {
                                        if (_newpass.text.isEmpty !=
                                            _confirmpass.text.isEmpty)
                                          setState(() {
                                            widget.check = false;
                                          });
                                        else {
                                          setState(() {
                                            widget.check = true;
                                          });
                                          info if1 = new info(
                                              _newusername.text,
                                              _newpass.text,
                                              "Người dung mới",
                                              "urlimage");
                                          Infos.add(if1);
                                        }
                                      }
                                    },
                                    child: Text(
                                      "Đăng ký",
                                      style: TextStyle(
                                          letterSpacing: 2,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white),
                                    )),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 30),
                              Text(
                                "Hoặc đăng ký bằng",
                                style: TextStyle(
                                    letterSpacing: 2,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey),
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 80),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    height: 35,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0xff97A4B7),
                                            offset: new Offset(8.0, 10.0),
                                            blurRadius: 25.0)
                                      ],
                                    ),
                                    child: IconButton(
                                      key: Key('shuffle_button'),
                                      onPressed: () {},
                                      iconSize: 15.0,
                                      icon: Icon(FontAwesomeIcons.mailBulk),
                                      color: Color(0xFF0A3068),
                                    ),
                                  ),
                                  Container(
                                    height: 35,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0xff97A4B7),
                                            offset: new Offset(8.0, 10.0),
                                            blurRadius: 25.0)
                                      ],
                                    ),
                                    child: IconButton(
                                      key: Key('repeat_button'),
                                      onPressed: () {},
                                      iconSize: 15.0,
                                      icon: Icon(FontAwesomeIcons.facebookF),
                                      color: Color(0xFF0A3068),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Widget SocialBoxSignInUpPass(
      BuildContext context, String category, TextEditingController a) {
    return Container(
      padding: EdgeInsets.only(left: 20),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 13,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xffE5F1FD),
          boxShadow: [
            BoxShadow(
                color: Color(0xff97A4B7),
                offset: new Offset(8.0, 10.0),
                blurRadius: 25.0)
          ]),
      child: Stack(
        children: [
          TextField(
              obscureText: !eye,
              controller: a,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: category,
                  hintStyle: TextStyle(
                      letterSpacing: 2,
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey))),
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
                icon: eye
                    ? Icon(
                        Icons.visibility,
                        color: Color(0xFF0A3068),
                        size: 20,
                      )
                    : Icon(
                        Icons.visibility_off,
                        color: Color(0xFF0A3068),
                        size: 20,
                      ),
                onPressed: () {
                  setState(() {
                    eye = !eye;
                  });
                }),
          )
        ],
      ),
    );
  }

  Widget SocialBoxSignInUpName(
      BuildContext context, String category, TextEditingController a) {
    return Container(
      padding: EdgeInsets.only(left: 20),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 13,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xffE5F1FD),
          boxShadow: [
            BoxShadow(
                color: Color(0xff97A4B7),
                offset: new Offset(8.0, 10.0),
                blurRadius: 25.0)
          ]),
      child: TextField(
          controller: a,
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: category,
              hintStyle: TextStyle(
                  letterSpacing: 2,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey))),
    );
  }
}
