import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';
import 'package:loadplaylist/PlayList/ListBaiHat.dart';
//import 'package:loadplaylist/PlayList/PlayLists.dart';
import 'package:loadplaylist/PlayList/Playlist.dart';
import 'file:///C:/Users/Admin/Downloads/music-master/lib/Views/Player.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:loadplaylist/main.dart';
import 'dart:math';
//import 'HomePage.dart';
import 'MiniPageMore.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class PlayListMusic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PlayListMusics('a', 'PlayList Bạn vừa nghe'
          //'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZOUWIWDU&ctime=1576115049&sig=bb95e683e36840bbc45310bdc01b136a67fd98ea217bedb362f8969bce8e381cebd63446f68a0b4e2d4e988421a9a10c841ef544ca477fa7d58ac53a9fa981ff&api_key=38e8643fb0dc04e8d65b99994d3dafff'
          ),
    );
  }
}

class PlayListMusics extends StatefulWidget {
  final String url;
  final String namePlaylist;
  PlayListMusics(this.url, this.namePlaylist);
  _PlayListMusicsState createState() => _PlayListMusicsState();
}

class _PlayListMusicsState extends State<PlayListMusics> {
  bool _loading = true;
  Random random = new Random();
  String link;
  //'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZOUWIWDU&ctime=1576115049&sig=bb95e683e36840bbc45310bdc01b136a67fd98ea217bedb362f8969bce8e381cebd63446f68a0b4e2d4e988421a9a10c841ef544ca477fa7d58ac53a9fa981ff&api_key=38e8643fb0dc04e8d65b99994d3dafff';
  String IDNhacTreTop100 = 'ZWZB969E';
  String IDNhacIDMTop100 = 'ZWZB969F';
  String IDNhacTruTinh100 = 'Z6CZOIWU';
  String IDNhacCachMangTop100 = 'ZWZB96AO';
  List<String> ListIDGoiY = [
    'ZU9EUF68',
    'ZU9EUF76',
    'ZUA67EDW',
    'ZU9DCU07',
    'ZU8IICE0',
    'ZU8ZUWC9',
    'ZU6ZEAZD',
    'ZUAF0ZWO',
    'ZU6BW90D',
    'ZOUWWOOO',
    'ZWZCOBEF',
    'ZOUWIWDU',
  ];
  Future<BaiHat> future;

//playlist pl=new playlist();
//    'Z6CZOIWU',
//    'IDM',
//    'https://photo-resize-zmp3.zadn.vn/w165_r1x1_jpeg/cover/3/1/8/f/318f209af4a9010695d12747f2c1d62a.jpg',
//    'https://photo-resize-zmp3.zadn.vn/w480_r1x1_jpeg/cover/3/1/8/f/318f209af4a9010695d12747f2c1d62a.jpg',
//    'Top 100 Nhạc EDM Việt là danh sách 100 ca khúc hot nhất hiện tại',
//    100);
  void loading() async {
    setState(() {
      _loading = true;
    });
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      _loading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    loading();

    link = this.widget.url;
    future = fetchAlbum(link);
  }

  @override
  Widget build(BuildContext context) {
    //future=pl.fetchAlbum(IDNhacTreTop100);
    return Scaffold(
      backgroundColor: Color(0xffE5F1FD),

      body: _loading
          ? Center(
              child: CircularProgressIndicator(
              strokeWidth: 3,
              backgroundColor: Color(0xFF0A3068),
            ))
          : SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 2 / 6 + 30,
                    child: Stack(
                      children: <Widget>[
// hinh anh đại diện lớn
                        Container(
                          height: MediaQuery.of(context).size.height * 2 / 6,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(50)),
                            color: Color(0xFF0A3068),
                            image: DecorationImage(
                                fit: BoxFit.fitWidth,
                                image: NetworkImage(playlist
                                    .listbh[random.nextInt(10)].hinhanhlon)),
                          ),
                        ),
                        Center(
                            child: Container(
                          height: MediaQuery.of(context).size.height / 10,
                          width: double.infinity,
                          color: Color(0xFF0A3068).withOpacity(.3),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                widget.namePlaylist,
                                style: TextStyle(
                                    letterSpacing: 2,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white),
                              ),
                              Text(
                                (playlist.listbh.length - 2).toString() +
                                    "  bài hát",
                                style: TextStyle(
                                    letterSpacing: 2,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white),
                              ),
                            ],
                          ),
                        )),
// nút play all
                        Positioned(
                          top: MediaQuery.of(context).size.height * 2 / 6 - 30,
                          right: MediaQuery.of(context).size.width / 2 - 30,
                          child: Container(
                            height: 60,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/images/disk.png"),
                                  fit: BoxFit.cover),
                              //color: Color(0xffE5F1FD),

                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff97A4B7),
                                    offset: new Offset(8.0, 10.0),
                                    blurRadius: 25.0)
                              ],
                            ),
                            child: IconButton(
                              key: Key('play_button'),
                              onPressed: () {
                                setState(() {
                                  Navigator.push(
                                      context,
                                      PageRouteBuilder(
                                          transitionDuration:
                                              Duration(seconds: 1),
                                          transitionsBuilder: (BuildContext
                                                  context,
                                              Animation<double> animation,
                                              Animation<double> secAnimation,
                                              Widget child) {
                                            animation = CurvedAnimation(
                                                parent: animation,
                                                curve: Curves.easeInOutBack);
                                            return ScaleTransition(
                                              alignment: Alignment.center,
                                              scale: animation,
                                              child: child,
                                            );
                                          },
                                          pageBuilder: (BuildContext context,
                                              Animation<double> animation,
                                              Animation<double> secAnimation) {
                                            return PlayerWidget(
                                                baihat: playlist.listbh[
                                                    random.nextInt(20)]);
                                          }));
                                });
                              },
                              iconSize: 22.0,
                              icon: Icon(Icons.play_arrow),
                              color: Color(0xFF0A3068),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0, left: 1),
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage("assets/images/disk.png"),
                                  fit: BoxFit.fill),
                              //color: Color(0xffE5F1FD),

                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff97A4B7),
                                    offset: new Offset(8.0, 10.0),
                                    blurRadius: 25.0)
                              ],
                            ),
                            child: IconButton(
                              key: Key('back_button'),
                              onPressed: () {
                                setState(() {
                                  Navigator.pop((context));
                                });
                              },
                              iconSize: 18.0,
                              icon: Icon(Icons.arrow_back_ios),
                              color: Color(0xFF0A3068),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 30),
                  Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'LIST MUSIC',
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF0A3068)),
                      ),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 50),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
//load list music
                    child: Container(
                      height: MediaQuery.of(context).size.height * 4 / 5 + 40,
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.only(
                          top: 15.0, bottom: 15, left: 5, right: 5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xff97A4B7),
                              offset: new Offset(8.0, 10.0),
                              blurRadius: 25.0)
                        ],
                      ),
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                        transitionDuration:
                                            Duration(seconds: 1),
                                        transitionsBuilder:
                                            (BuildContext context,
                                                Animation<double> animation,
                                                Animation<double> secAnimation,
                                                Widget child) {
                                          animation = CurvedAnimation(
                                              parent: animation,
                                              curve: Curves.easeInOutBack);
                                          return ScaleTransition(
                                            alignment: Alignment.center,
                                            scale: animation,
                                            child: child,
                                          );
                                        },
                                        pageBuilder: (BuildContext context,
                                            Animation<double> animation,
                                            Animation<double> secAnimation) {
                                          return PlayerWidget(
                                              baihat: playlist.listbh[index]);
                                        }));
                              });
                            },
                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  width: double.infinity,
                                  height:
                                      MediaQuery.of(context).size.height / 10,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          (index + 1).toString(),
                                          style: TextStyle(
                                              letterSpacing: 2,
                                              fontSize: 10,
                                              fontWeight: FontWeight.w400,
                                              color: Color(0xFF0A3068)),
                                        ),
                                        Container(
                                          width: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  10 -
                                              30,
                                          height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  10 -
                                              30,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: Color(0xFF0A3068),
                                                width: 2),
                                            image: DecorationImage(
                                                fit: BoxFit.cover,
                                                image: NetworkImage(playlist
                                                    .listbh[index].hinhanhlon)),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Flexible(
                                              child: Container(
                                                padding: const EdgeInsets.only(
                                                    bottom: 5),
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    2 /
                                                    3,
                                                child: Text(
                                                    playlist.listbh[index].ten,
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                        letterSpacing: 1.2,
                                                        fontSize: 13,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color:
                                                            Color(0xFF0A3068))),
                                              ),
                                            ),
                                            Flexible(
                                              child: Text(
                                                  playlist.listbh[index].casi,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      letterSpacing: 1.2,
                                                      fontSize: 10,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Colors.grey)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: IconButton(
                                      icon: Icon(Icons.more_vert,
                                          color: Color(0xFF0A3068)),
                                      onPressed: () {
                                        MiniPageMore(
                                            context, playlist.listbh[index],
                                            () {
                                          setState(() {
                                            BaiHat bh1 = new BaiHat(
                                                playlist.listbh[index].id,
                                                playlist.listbh[index].ten,
                                                playlist.listbh[index].casi,
                                                playlist.listbh[index].hinhanh,
                                                playlist
                                                    .listbh[index].hinhanhlon,
                                                playlist
                                                    .listbh[index].linkLyric,
                                                playlist.listbh[index].duration
                                                    .toString());
                                            int check = 0;
                                            for (int i = 0;
                                                i < SongInPlayList.length;
                                                i++) {
                                              if (bh1.id ==
                                                  SongInPlayList[i].id) check++;
                                            }
                                            if (check == 0) {
                                              SongInPlayList.add(bh1);
                                            }

                                            Navigator.pop(context);
                                          });
                                        }, playlist.listbh[index]);
                                      }),
                                ),
                              ],
                            ),
                          );
                        },
                        itemCount: playlist.listbh.length - 2,
                      ),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 25),
//nghệ sĩ tham gia
                  Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'NGHỆ SĨ THAM GIA',
                              style: TextStyle(
                                  letterSpacing: 2,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF0A3068)),
                            ),
                          ),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height / 40),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          height: MediaQuery.of(context).size.height / 4,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return _MakeCardsingle(
                                  playlist.listbh[index].hinhanhlon,
                                  playlist.listbh[index].casi);
                            },
                            itemCount: playlist.listbh.length,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),

//      floatingActionButton: Container(
//        height: 50,
//        width: 50,
//        child: FloatingActionButton(
//          onPressed: () {
//            setState(() {
//              Navigator.push(context,
//                  MaterialPageRoute(builder: (context) => MyHomePage(2)));
//            });
//          },
//          child: Icon(
//            Icons.home,
//            color: Colors.white,
//          ),
//          backgroundColor: Color(0xFF0A3068),
//        ),
//      g
    );
  }

  Widget _MakeCardsingle(String usImage, String casi) {
    return AspectRatio(
      aspectRatio: 1.5 / 1.5,
      child: Container(
        height: 300,
        width: 300,
        margin: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.white.withOpacity(.3), width: 4),
            image: DecorationImage(
                image: NetworkImage(
                  usImage,
                ),
                fit: BoxFit.cover)),
        child: Align(
            alignment: Alignment.bottomLeft,
            child: Text(casi,
                style: TextStyle(
                    letterSpacing: 1.2,
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Colors.white))),
      ),
    );
  }
}
