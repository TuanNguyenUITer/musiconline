import 'package:flutter/material.dart';
import 'package:loadplaylist/API/Api.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';
import 'package:loadplaylist/PlayList/ListBaiHat.dart';
import 'package:loadplaylist/PlayList/Playlist.dart';

import 'MiniPageMore.dart';
import 'Player.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Search("fdfdf"),
    );
  }
}

class Search extends StatefulWidget {
  String textsearch;
  Search(this.textsearch);
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  bool isLoading = false;
  TextEditingController _textsearch = new TextEditingController();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() => isLoading = true);
      await Api().searchBaiHat(widget.textsearch, onSuccess: (values) {
        setState(() {
          isLoading = false;
          isLoading = false;
          playlist.listbh = values;
        });
      }, onError: (msg) {
        setState(() => isLoading = false);
        print(msg);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0A3068),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height / 45,
                ),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 15,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 15,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            }),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 5 / 6 - 4,
                        height: MediaQuery.of(context).size.height / 15,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Color(0xffE5F1FD),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: TextField(
                            controller: _textsearch,
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                suffixIcon: IconButton(
                                    icon: Icon(
                                      Icons.search,
                                      color: Color(0xFF0A3068),
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                Search(_textsearch.text),
                                          ));
                                    }),
                                hintText: "Search",
                                hintStyle: TextStyle(
                                    color: Color(0xFF0A3068), fontSize: 15)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: MediaQuery.of(context).size.height / 20,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Kết quả tìm kiếm: ",
                    style: TextStyle(
                      color: Colors.white,
                      letterSpacing: 2,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 45,
                ),
                playlist.listbh.length != null
                    ? Center(
                        child: Container(
                          height: MediaQuery.of(context).size.height -
                              MediaQuery.of(context).size.height / 4,
                          padding: const EdgeInsets.only(
                              top: 15.0, bottom: 15, left: 5, right: 5),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xff97A4B7),
                                  offset: new Offset(8.0, 10.0),
                                  blurRadius: 25.0)
                            ],
                          ),
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  setState(() {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => PlayerWidget(
                                              baihat: playlist.listbh[index]),
                                        ));
                                  });
                                },
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      width: double.infinity,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              10,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.white,
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: <Widget>[
// số thứ tywj trong listmusic
                                            Text(
                                              (index + 1).toString(),
                                              style: TextStyle(
                                                  letterSpacing: 2,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xFF0A3068)),
                                            ),
                                            //hinh anh trong listmusci
                                            Container(
                                              width: MediaQuery.of(context)
                                                          .size
                                                          .height /
                                                      10 -
                                                  30,
                                              height: MediaQuery.of(context)
                                                          .size
                                                          .height /
                                                      10 -
                                                  30,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                    color: Color(0xFF0A3068),
                                                    width: 2),
                                                image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: NetworkImage(playlist
                                                        .listbh[index]
                                                        .hinhanhlon)),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            //tên bài hát và tên ca si
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Flexible(
                                                  child: Text(
                                                      playlist
                                                          .listbh[index].ten,
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          letterSpacing: 1.2,
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Color(
                                                              0xFF0A3068))),
                                                ),
                                                Flexible(
                                                  child: Text(
                                                      playlist
                                                          .listbh[index].casi,
                                                      style: TextStyle(
                                                          letterSpacing: 1.2,
                                                          fontSize: 10,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          color: Colors.grey)),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    //button more
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: IconButton(
                                        icon: Icon(Icons.more_vert,
                                            color: Color(0xFF0A3068)),
                                        onPressed: () {
                                          MiniPageMore(
                                              context, playlist.listbh[index],
                                              () {
                                            setState(() {
                                              BaiHat bh1 = new BaiHat(
                                                  playlist.listbh[index].id,
                                                  playlist.listbh[index].ten,
                                                  playlist.listbh[index].casi,
                                                  playlist
                                                      .listbh[index].hinhanh,
                                                  playlist
                                                      .listbh[index].hinhanhlon,
                                                  playlist
                                                      .listbh[index].linkLyric,
                                                  playlist
                                                      .listbh[index].duration
                                                      .toString());
                                              int check = 0;
                                              for (int i = 0;
                                                  i < SongInPlayList.length;
                                                  i++) {
                                                if (bh1.id ==
                                                    SongInPlayList[i].id)
                                                  check++;
                                              }
                                              if (check == 0) {
                                                SongInPlayList.add(bh1);
                                              }

                                              Navigator.pop(context);
                                            });
                                          }, playlist.listbh[index]);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                            itemCount: playlist.listbh.length,
                          ),
                        ),
                      )
                    : Center(
                        child: Text(
                          "Không tìm thấy kết quả!",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
//              Container(
//                height: MediaQuery.of(context).size.height * 2 / 3,
//                child: ListView.builder(
//                  itemBuilder: (context, index) {
//                    return Text(playlist.listbh[index].ten);
//                  },
//                  itemCount: playlist.listbh.length,
//                ),
//              ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
