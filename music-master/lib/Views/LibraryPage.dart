import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';
import 'package:loadplaylist/PlayList/LibraryPlayList.dart';
import 'package:loadplaylist/PlayList/ListBaiHat.dart';

//import 'package:loadplaylist/PlayList/PlayLists.dart';
import 'package:loadplaylist/PlayList/Playlist.dart';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'SearchPages.dart';
import 'file:///C:/Users/Admin/Downloads/music-master/lib/Views/Player.dart';
import 'package:loadplaylist/Views/PlayListMusic.dart';

Future<BaiHat> fetchAlbum(String linkPlaylist) async {
  final response = await http.get(linkPlaylist);
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    final jsondecode = json.decode(response.body);
    final data = jsondecode['data']['song']['items'];
    playlist.listbh = new List();

    for (Map i in data) {
      playlist.listbh.add(BaiHat.fromJson(i));
    }
    return BaiHat.fromJson(data[0]);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class LibraryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LibraryPages(),
    );
  }
}

class LibraryPages extends StatefulWidget {
  @override
  _LibraryPagesState createState() => _LibraryPagesState();
}

class _LibraryPagesState extends State<LibraryPages> {
  int lengthPlayList = 0;
  Random random = new Random();
  String link =
      'https://zingmp3.vn/api/playlist/get-playlist-detail?id=ZOUWIWDU&ctime=1576115049&sig=bb95e683e36840bbc45310bdc01b136a67fd98ea217bedb362f8969bce8e381cebd63446f68a0b4e2d4e988421a9a10c841ef544ca477fa7d58ac53a9fa981ff&api_key=38e8643fb0dc04e8d65b99994d3dafff';
  Future<BaiHat> future;
  @override
  void initState() {
    super.initState();

    future = fetchAlbum(link);
  }

  TextEditingController _textsearch = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF0A3068),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height / 35,
                ),
// thanh tim kiem
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 15,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Color(0xffE5F1FD),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: TextField(
                      controller: _textsearch,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: IconButton(
                              icon: Icon(
                                Icons.search,
                                color: Color(0xFF0A3068),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          Search(_textsearch.text),
                                    ));
                              }),
                          hintText: "Search",
                          hintStyle: TextStyle(
                              color: Color(0xFF0A3068), fontSize: 15)),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 60,
                ),
                // poster library
                Stack(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height / 3 - 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              image:
                                  AssetImage("assets/images/imageLibrary.png"),
                              fit: BoxFit.cover)),
                    ),
                    Positioned(
                      top: MediaQuery.of(context).size.height / 8,
                      left: MediaQuery.of(context).size.width / 12,
                      child: Text(
                        "Nghe nhạc bất tận",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 2,
                            fontSize: MediaQuery.of(context).size.height / 50),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 35,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Thư viện",
                      style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width / 40,
                    ),
                    Icon(Icons.library_music, color: Colors.white)
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 60,
                ),
//bài hát thư viện

                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xff97A4B7),
                            offset: new Offset(8.0, 10.0),
                            blurRadius: 25.0)
                      ],
                      color: Color(0xffE5F1FD),
                      borderRadius: BorderRadius.circular(15)),
                  child: ExpansionTile(
                    leading: Container(
                      height: 60,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xFF0A3068),
                      ),
                      child: Icon(
                        Icons.music_note,
                        color: Colors.white,
                      ),
                    ),
                    title: Text(
                      "Bài hát",
                      style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF0A3068)),
                    ),
                    trailing: IconButton(
                      icon: Icon(
                        Icons.arrow_drop_down,
                        color: Color(0xFF0A3068),
                        size: 24,
                      ),
                    ),
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 4 / 5,
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.only(
                            top: 15.0, bottom: 15, left: 5, right: 5),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff97A4B7),
                                offset: new Offset(8.0, 10.0),
                                blurRadius: 25.0)
                          ],
                        ),
                        child: Column(
                          children: [
                            Container(
                              height: 40,
                              width: MediaQuery.of(context).size.width / 2,
                              decoration: BoxDecoration(
                                color: Color(0xffE5F1FD),
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(4.0, 4),
                                      blurRadius: 10.0)
                                ],
                              ),
                              child: FlatButton(
                                key: Key(''),
                                onPressed: () {
                                  setState(() {
                                    Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                            transitionDuration:
                                                Duration(seconds: 1),
                                            transitionsBuilder: (BuildContext
                                                    context,
                                                Animation<double> animation,
                                                Animation<double> secAnimation,
                                                Widget child) {
                                              animation = CurvedAnimation(
                                                  parent: animation,
                                                  curve: Curves.easeInOutBack);
                                              return ScaleTransition(
                                                alignment: Alignment.center,
                                                scale: animation,
                                                child: child,
                                              );
                                            },
                                            pageBuilder: (BuildContext context,
                                                Animation<double> animation,
                                                Animation<double>
                                                    secAnimation) {
                                              return PlayerWidget(
                                                  baihat: playlist.listbh[
                                                      random.nextInt(playlist
                                                              .listbh.length -
                                                          2)]);
                                            }));
                                  });
                                },
                                child: Text(
                                  "Phát ngẫu nhiên",
                                  style: TextStyle(
                                      letterSpacing: 1.5,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xFF0A3068)),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              height:
                                  MediaQuery.of(context).size.height * 3 / 4 -
                                      60,
                              child: ListView.builder(
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        Navigator.push(
                                            context,
                                            PageRouteBuilder(
                                                transitionDuration:
                                                    Duration(seconds: 1),
                                                transitionsBuilder:
                                                    (BuildContext context,
                                                        Animation<double>
                                                            animation,
                                                        Animation<double>
                                                            secAnimation,
                                                        Widget child) {
                                                  animation = CurvedAnimation(
                                                      parent: animation,
                                                      curve:
                                                          Curves.easeInOutBack);
                                                  return ScaleTransition(
                                                    alignment: Alignment.center,
                                                    scale: animation,
                                                    child: child,
                                                  );
                                                },
                                                pageBuilder: (BuildContext
                                                        context,
                                                    Animation<double> animation,
                                                    Animation<double>
                                                        secAnimation) {
                                                  return PlayerWidget(
                                                      baihat: playlist
                                                          .listbh[index]);
                                                }));
                                      });
                                    },
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(top: 10),
                                          width: double.infinity,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              10,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: Colors.white,
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: <Widget>[
                                                Text(
                                                  (index + 1).toString(),
                                                  style: TextStyle(
                                                      letterSpacing: 2,
                                                      fontSize: 10,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: Color(0xFF0A3068)),
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                              .size
                                                              .height /
                                                          10 -
                                                      30,
                                                  height: MediaQuery.of(context)
                                                              .size
                                                              .height /
                                                          10 -
                                                      30,
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        color:
                                                            Color(0xFF0A3068),
                                                        width: 2),
                                                    image: DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: NetworkImage(
                                                            playlist
                                                                .listbh[index]
                                                                .hinhanhlon)),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Text(
                                                          playlist.listbh[index]
                                                              .ten,
                                                          textAlign:
                                                              TextAlign.left,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style: TextStyle(
                                                              letterSpacing:
                                                                  1.2,
                                                              fontSize: 13,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              color: Color(
                                                                  0xFF0A3068))),
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                          playlist.listbh[index]
                                                              .casi,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style: TextStyle(
                                                              letterSpacing:
                                                                  1.2,
                                                              fontSize: 10,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              color:
                                                                  Colors.grey)),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Align(
                                          alignment: Alignment.centerRight,
                                          child: IconButton(
                                              icon: Icon(Icons.delete_outline,
                                                  size: 20,
                                                  color: Color(0xFF0A3068)),
                                              onPressed: () {}),
                                        ),
                                      ],
                                    ),
                                  );
                                },
                                itemCount: playlist.listbh.length - 2,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 35,
                ),
// playlist thư viện
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xff97A4B7),
                            offset: new Offset(8.0, 10.0),
                            blurRadius: 25.0)
                      ],
                      color: Color(0xffE5F1FD),
                      borderRadius: BorderRadius.circular(15)),
                  child: ExpansionTile(
                      leading: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFF0A3068),
                        ),
                        child: Icon(
                          Icons.queue_music,
                          color: Colors.white,
                        ),
                      ),
                      title: Text(
                        "Bài hát bạn yêu thích ",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF0A3068)),
                      ),
                      trailing: IconButton(
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Color(0xFF0A3068),
                          size: 24,
                        ),
                      ),
                      children: [
                        Container(
                            height: MediaQuery.of(context).size.height * 3 / 4,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height *
                                            2 /
                                            3 -
                                        MediaQuery.of(context).size.height / 10,
                                    child: ListView.builder(
                                      itemBuilder: (context, index) {
                                        return GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        PlayerWidget(
                                                            baihat:
                                                                SongInPlayList[
                                                                    index]),
                                                  ));
                                            });
                                          },
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: <Widget>[
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 10),
                                                width: double.infinity,
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    10,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Colors.white,
                                                ),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Text(
                                                        (index + 1).toString(),
                                                        style: TextStyle(
                                                            letterSpacing: 2,
                                                            fontSize: 10,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: Color(
                                                                0xFF0A3068)),
                                                      ),
                                                      Container(
                                                        width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height /
                                                                10 -
                                                            30,
                                                        height: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height /
                                                                10 -
                                                            30,
                                                        decoration:
                                                            BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          border: Border.all(
                                                              color: Color(
                                                                  0xFF0A3068),
                                                              width: 2),
                                                          image: DecorationImage(
                                                              fit: BoxFit.cover,
                                                              image: NetworkImage(
                                                                  SongInPlayList[
                                                                          index]
                                                                      .hinhanhlon)),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Flexible(
                                                            child: Container(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      bottom:
                                                                          5),
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width *
                                                                  2 /
                                                                  3,
                                                              child: Text(
                                                                  SongInPlayList[index]
                                                                      .ten,
                                                                  textAlign:
                                                                      TextAlign
                                                                          .left,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis,
                                                                  style: TextStyle(
                                                                      letterSpacing:
                                                                          1.2,
                                                                      fontSize:
                                                                          13,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400,
                                                                      color: Color(
                                                                          0xFF0A3068))),
                                                            ),
                                                          ),
                                                          Flexible(
                                                            child: Text(
                                                                SongInPlayList[
                                                                        index]
                                                                    .casi,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: TextStyle(
                                                                    letterSpacing:
                                                                        1.2,
                                                                    fontSize:
                                                                        10,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    color: Colors
                                                                        .grey)),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: IconButton(
                                                    icon: Icon(
                                                        Icons.delete_sweep,
                                                        color:
                                                            Color(0xFF0A3068)),
                                                    onPressed: () {
                                                      setState(() {
                                                        SongInPlayList.removeAt(
                                                            index);
                                                      });
                                                    }),
                                              ),
                                            ],
                                          ), /*CardPlayList(
                                            context,
                                            LibraryPL[index].GetID.toString(),
                                            LibraryPL[index].GetName,
                                            LibraryPL[index].SoSong.toString())*/
                                        );
                                      },
                                      itemCount: SongInPlayList.length,
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      ]),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 35,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Nghe gần đây",
                      style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width / 40,
                    ),
                    Icon(Icons.headset_mic, color: Colors.white)
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height / 60,
                ),
//   nghe gần đây
                Container(
                  height: MediaQuery.of(context).size.height * 3 / 4,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xff97A4B7),
                            offset: new Offset(8.0, 10.0),
                            blurRadius: 25.0)
                      ],
                      color: Color(0xffE5F1FD),
                      borderRadius: BorderRadius.circular(20)),
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => PlayListMusics(
                                      LibraryPL[index].UrlPlayList,
                                      LibraryPL[index].GetName),
                                ));
                          });
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Container(
                              margin:
                                  EdgeInsets.only(top: 10, left: 10, right: 10),
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height / 10,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      (index + 1).toString(),
                                      style: TextStyle(
                                          letterSpacing: 2,
                                          fontSize: 10,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF0A3068)),
                                    ),
                                    Container(
                                      width:
                                          MediaQuery.of(context).size.height /
                                                  10 -
                                              10,
                                      height:
                                          MediaQuery.of(context).size.height /
                                                  10 -
                                              10,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: NetworkImage(
                                                LibraryPL[index].UrlImage)),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Flexible(
                                          child: Container(
                                            padding: const EdgeInsets.only(
                                                bottom: 5),
                                            width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    1 /
                                                    2 +
                                                20,
                                            child: Text(
                                                LibraryPL[index].GetName,
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    letterSpacing: 1.2,
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.w400,
                                                    color: Color(0xFF0A3068))),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ), /*CardPlayList(
                                            context,
                                            LibraryPL[index].GetID.toString(),
                                            LibraryPL[index].GetName,
                                            LibraryPL[index].SoSong.toString())*/
                      );
                    },
                    itemCount: LibraryPL.length,
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  // card playlist nghe gần đây
  Widget CardPlayList(
      BuildContext context, String str1, String str2, String str3) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        height: MediaQuery.of(context).size.height / 8,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 8 - 20,
              width: MediaQuery.of(context).size.height / 8 - 20,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.amber,
                  image: DecorationImage(
                      image: NetworkImage(str1), fit: BoxFit.cover)),
              child: Center(
                  child: Icon(
                Icons.music_note,
                color: Color(0xFF0A3068),
              )),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 15,
            ),
            Container(
              width: MediaQuery.of(context).size.width / 2 - 30,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Text(str1,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF0A3068))),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 120,
                  ),
                  Flexible(
                    child: Text(str3 + " - " + str2,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey)),
                  ),
                ],
              ),
            ),
            Container(
              height: 30,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Color(0xff97A4B7),
                      offset: new Offset(8.0, 10.0),
                      blurRadius: 25.0)
                ],
              ),
              child: IconButton(
                key: Key('play_button'),
                onPressed: () {},
                iconSize: 15.0,
                icon: Icon(Icons.play_circle_filled),
                color: Color(0xFF0A3068),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget MiniPageAddPlayList(BuildContext context) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext more) {
          return AnimatedContainer(
            duration: Duration(seconds: 1),
            curve: Curves.fastOutSlowIn,
            margin: const EdgeInsets.only(
                top: 10.0, right: 10, left: 10, bottom: 200),
            height: MediaQuery.of(context).size.height * 2 / 5,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Color(0xff97A4B7),
                      offset: new Offset(8.0, 10.0),
                      blurRadius: 25.0)
                ],
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 15,
                ),
                SocialBox("Nhập tên playlist"),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 17,
                  width: MediaQuery.of(context).size.width / 4,
                  decoration: BoxDecoration(
                      color: Color(0xFF0A3068),
                      borderRadius: BorderRadius.circular(15)),
                  child: FlatButton(
                      onPressed: () {
                        setState(() {
                          /*LibraryPlayList lpl1 =
                              new LibraryPlayList(1, "PLay1", "sdfg", 0);
                          LibraryPL.add(lpl1); */
                          BaiHat bh1 = new BaiHat("d", "dss", "123", "sd", "dn",
                              "djs", 12.toString());
                          SongInPlayList.add(bh1);
                          Navigator.pop(context);
                        });
                      },
                      child: Text(
                        "Lưu",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                      )),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          );
        });
  }
}

class SocialBox extends StatelessWidget {
  final String category;

  const SocialBox(
    this.category,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20),
      width: MediaQuery.of(context).size.width * 2 / 3,
      height: MediaQuery.of(context).size.height / 15,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xffE5F1FD).withOpacity(.8),
          boxShadow: [
            BoxShadow(
                color: Color(0xff97A4B7),
                offset: new Offset(8.0, 10.0),
                blurRadius: 25.0)
          ]),
      child: TextField(
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: category,
              hintStyle: TextStyle(
                  letterSpacing: 2,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey))),
    );
  }
}
