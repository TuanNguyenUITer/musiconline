import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'dart:math';
import 'package:loadplaylist/PlayList/ListBaiHat.dart';
import 'package:loadplaylist/API/Api.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';
import 'package:loadplaylist/Views/MiniPageMore.dart';
import 'package:loadplaylist/main.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:loadplaylist/PlayList/Playlist.dart';
import 'package:http/http.dart' as http;
import 'package:loadplaylist/PlayList/ListBaiHat.dart';
import 'package:flutter/services.dart';

import 'InformationPage.dart';

enum PlayerState { stopped, playing, paused }
enum PlayingRouteState { speakers, earpiece }

class PlayerWidget extends StatefulWidget {
  //final String url;
  final PlayerMode mode;
  BaiHat baihat;

  PlayerWidget(
      {Key key, @required this.baihat, this.mode = PlayerMode.MEDIA_PLAYER})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PlayerWidgetState(baihat, mode);
  }
}

class _PlayerWidgetState extends State<PlayerWidget>
    with TickerProviderStateMixin {
  // quảng cáo

  Animation _arrowAnimation;
  AnimationController _arrowAnimationController;

  PlayerMode mode;
  BaiHat baiHat;
  //String url=this.baiHat.id;
  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;

  PlayerState _playerState = PlayerState.stopped;
  PlayingRouteState _playingRouteState = PlayingRouteState.speakers;
  StreamSubscription _durationSubscription;
  StreamSubscription _positionSubscription;
  StreamSubscription _playerCompleteSubscription;
  StreamSubscription _playerErrorSubscription;
  StreamSubscription _playerStateSubscription;

  get _isPlaying => _playerState == PlayerState.playing;
  get _isPaused => _playerState == PlayerState.paused;
  get _durationText => _duration?.toString()?.split('.')?.first ?? '';
  get _positionText => _position?.toString()?.split('.')?.first ?? '';

  get _isPlayingThroughEarpiece =>
      _playingRouteState == PlayingRouteState.earpiece;

  _PlayerWidgetState(this.baiHat, this.mode);
  String status = 'hidden';
  var lyric = "";
  bool lyricLoading = false;
  @override
  void initState() {
    super.initState();
//quảng cáo

    //
    _initAudioPlayer();

    _play();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setState(() => lyricLoading = true);
      await Api().LoiBaiHat(baiHat.linkLyric, onSuccess: (values) {
        setState(() {
          lyricLoading = false;
          lyric = values;
        });
      }, onError: (msg) {
        setState(() => lyricLoading = false);
        print(msg);
      });
    });

    _arrowAnimationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 400000));
    _arrowAnimation =
        Tween(begin: 0.0, end: 4 * pi).animate(_arrowAnimationController);
    _arrowAnimationController.isCompleted
        ? _arrowAnimationController.reverse()
        : _arrowAnimationController.forward();
  }

  //

  //

  @override
  void dispose() {
    _audioPlayer.stop();
    _durationSubscription?.cancel();
    _positionSubscription?.cancel();
    _playerCompleteSubscription?.cancel();
    _playerErrorSubscription?.cancel();
    _playerStateSubscription?.cancel();
    super.dispose();
  }

  Random random = new Random();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xFF0A3068),

        body: SlidingUpPanel(
          renderPanelSheet: false,
          minHeight: MediaQuery.of(context).size.height / 17,
          maxHeight: MediaQuery.of(context).size.height,
          panel: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter: ColorFilter.mode(
                        Color(0xffE5F1FD).withOpacity(0.1), BlendMode.dstATop),
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      baiHat.hinhanhlon,
                    )),
                borderRadius: BorderRadius.circular(24),
                color: Colors.white,
                border: Border.all(
                    width: 2,
                    color:
                        Colors.white //                   <--- border width here
                    ),
              ),
              margin: const EdgeInsets.fromLTRB(10.0, 25.0, 10.0, 10.0),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Stack(
                      children: [
                        Slider(
                          activeColor: Color(0xFF0A3068),
                          onChanged: (v) {
                            final Position = v * _duration.inMilliseconds;

                            _audioPlayer
                                .seek(Duration(milliseconds: Position.round()));
                          },
                          value: (_position != null &&
                                  _duration != null &&
                                  _position.inMilliseconds > 0 &&
                                  _position.inMilliseconds <
                                      _duration.inMilliseconds)
                              ? _position.inMilliseconds /
                                  _duration.inMilliseconds
                              : 0.0,
                        ),
                      ],
                    ),
                  ),
                  Center(
                    child: Text(
                      baiHat.ten,
                      style: TextStyle(
                          letterSpacing: 1,
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF0A3068)),
                    ),
                  ),
                  Center(
                    child: Text(
                      baiHat.casi,
                      style: TextStyle(
                          letterSpacing: 1.1,
                          fontSize: 11,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 30,
                  ),
                  Center(
                    child: Text(
                      "Lyric",
                      style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF0A3068)),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 40,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 2 / 3,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(25.0),
                          child: Flexible(
                            child: Text(
                              lyric,
                              //"Lorem Ipsum chỉ đơn giản\n là văn bản giả của ngành công nghiệp in ấn và \nsắp chữ. Lorem Ipsum đã trở thành văn bản giả tiêu chuẩn\n của ngành công nghiệp kể từ những năm 1500, khi một nhà in không rõ đã lấy một loại galley và xáo trộn nó để tạo ra một cuốn sách mẫu vật. Nó đã tồn tại không chỉ năm thế kỷ, mà còn là bước nhảy vọt trong việc sắp chữ điện tử, về cơ bản vẫn không thay đổi. Nó được phổ biến vào những năm 1960 với việc phát hành các tờ Letraset có chứa các đoạn Lorem Ipsum và gần đây với phần mềm xuất bản trên máy tính để bàn như Aldus PageMaker bao gồm các phiên bản của Lorem Ipsum.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  height: 2,
                                  letterSpacing: 1,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF0A3068)),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              )),
          collapsed: Container(
            margin: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.0),
                  topRight: Radius.circular(24.0)),
              color: Colors.white,
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 4,
                    width: MediaQuery.of(context).size.width / 3,
                    decoration: BoxDecoration(
                        color: Color(0xFF0A3068),
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  Text(
                    "Lyric",
                    style: TextStyle(
                        letterSpacing: 2,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF0A3068)),
                  ),
                ],
              ),
            ),
          ),
          body:
              //page playmusic
              Container(
            height: double.infinity,
            color: Color(0xFF0A3068),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height / 35),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
//button back
                      Container(
                        height: MediaQuery.of(context).size.height / 19,
                        width: MediaQuery.of(context).size.height / 18,
                        decoration: BoxDecoration(color: Color(0xFF0A3068)),
                      ),
                      Text(
                        "PLAYING NOW",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Color(0xffE5F1FD)),
                      ),
//button more
                      Container(
                        height: MediaQuery.of(context).size.height / 19,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff97A4B7),
                                offset: new Offset(8.0, 10.0),
                                blurRadius: 25.0)
                          ],
                        ),
                        child: IconButton(
                          key: Key('more_button'),
                          onPressed: () {
                            MiniPageMore(context, baiHat, () {
                              setState(() {
                                BaiHat bh1 = new BaiHat(
                                    baiHat.id,
                                    baiHat.ten,
                                    baiHat.casi,
                                    baiHat.hinhanh,
                                    baiHat.hinhanhlon,
                                    baiHat.linkLyric,
                                    baiHat.duration.toString());
                                int check = 0;
                                for (int i = 0;
                                    i < SongInPlayList.length;
                                    i++) {
                                  if (bh1.id == SongInPlayList[i].id) check++;
                                }
                                if (check == 0) {
                                  SongInPlayList.add(bh1);
                                }

                                Navigator.pop(context);
                              });
                            }, baiHat);
                          },
                          iconSize: 16.0,
                          icon: Icon(Icons.more_vert),
                          color: Color(0xFF0A3068),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 60),
                  Container(
                    decoration: BoxDecoration(
                        color: Color(0xffE5F1FD),
                        borderRadius: BorderRadius.circular(30)),
                    height: MediaQuery.of(context).size.height * 4 / 5,
                    child: Column(
                      children: [
                        SizedBox(
                            height: MediaQuery.of(context).size.height / 20),
//animate xoay hinh anh
                        AnimatedBuilder(
                          animation: _arrowAnimationController,
                          builder: (context, child) => Transform.rotate(
                            angle: _arrowAnimation.value,
                            child: Container(
                              height: MediaQuery.of(context).size.height / 3,
                              width: MediaQuery.of(context).size.height / 3,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 15.0)
                                ],
                                border: Border.all(
                                    color: Color(0xFF0A3068).withOpacity(.5),
                                    width: 4),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                      baiHat.hinhanhlon,
                                    )),
                                //'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true'
                              ),
                            ),
                          ),
                        ),

                        SizedBox(
                            height: MediaQuery.of(context).size.height / 20),
//load ten bài hát va tên casi
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Center(
                              child: Text(
                                baiHat.ten,
                                style: TextStyle(
                                    letterSpacing: 1,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFF0A3068)),
                              ),
                            ),
                            Center(
                              child: Text(
                                baiHat.casi,
                                style: TextStyle(
                                    letterSpacing: 1.1,
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey),
                              ),
                            ),
//slider load thời gian
                            Padding(
                              padding: EdgeInsets.all(4.0),
                              child: Stack(
                                children: [
                                  Slider(
                                    activeColor: Color(0xFF0A3068),
                                    onChanged: (v) {
                                      final Position =
                                          v * _duration.inMilliseconds;

                                      _audioPlayer.seek(Duration(
                                          milliseconds: Position.round()));
                                    },
                                    value: (_position != null &&
                                            _duration != null &&
                                            _position.inMilliseconds > 0 &&
                                            _position.inMilliseconds <
                                                _duration.inMilliseconds)
                                        ? _position.inMilliseconds /
                                            _duration.inMilliseconds
                                        : 0.0,
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              _position != null
                                  ? '${_positionText ?? ''} / ${_durationText ?? ''}'
                                  : _duration != null ? _durationText : '',
                              style: TextStyle(
                                  letterSpacing: 1.2,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF0A3068)),
                            ),
                          ],
                        ),
// dãy button 1
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              height: 35,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                key: Key('shuffle_button'),
                                onPressed: () {},
                                iconSize: 15.0,
                                icon: Icon(Icons.shuffle),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                            Container(
                              height: 35,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                key: Key('repeat_button'),
                                onPressed: () {
                                  _stop();
                                },
                                iconSize: 15.0,
                                icon: Icon(Icons.repeat),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                            Container(
                              height: 35,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                onPressed: _earpieceOrSpeakersToggle,
                                iconSize: 15.0,
                                icon: _isPlayingThroughEarpiece
                                    ? Icon(Icons.volume_up)
                                    : Icon(Icons.hearing),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height / 20),
//dãy button 2
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                key: Key('skip_previous_button'),
                                onPressed: () {
                                  Navigator.pop((context));
                                },
                                iconSize: 18.0,
                                icon: Icon(Icons.skip_previous),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                key: Key('pause_button'),
                                onPressed: _isPlaying ? () => _pause() : null,
                                iconSize: 18.0,
                                icon: Icon(Icons.pause),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                            Container(
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                key: Key('play_button'),
                                onPressed: _isPlaying ? null : () => _play(),
                                iconSize: 20.0,
                                icon: Icon(Icons.play_arrow),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                key: Key('stop_button'),
                                onPressed: _isPlaying || _isPaused
                                    ? () => _stop()
                                    : null,
                                iconSize: 18.0,
                                icon: Icon(Icons.stop),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xff97A4B7),
                                      offset: new Offset(8.0, 10.0),
                                      blurRadius: 25.0)
                                ],
                              ),
                              child: IconButton(
                                key: Key('next_button'),
                                onPressed: () {
                                  _stop();
                                  setState(() {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => PlayerWidget(
                                              baihat: playlist.listbh[
                                                  random.nextInt(
                                                      playlist.listbh.length -
                                                          2)]),
                                        ));
                                  });
                                },
                                iconSize: 18.0,
                                icon: Icon(Icons.skip_next),
                                color: Color(0xFF0A3068),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),

        //floating button
        floatingActionButton: SpeedDial(
          // both default to 16
          marginRight: 18,
          marginBottom: 20,
          animatedIcon: AnimatedIcons.menu_close,
          animatedIconTheme: IconThemeData(size: 20.0, color: Colors.white),

          closeManually: false,
          curve: Curves.easeInCirc,
          overlayColor: Color(0xFF0A3068),
          overlayOpacity: 0.2,
          onOpen: () {
            setState(() {
              _pause();
            });
          },
          onClose: () {},
          tooltip: 'Speed Dial',
          heroTag: 'speed-dial-hero-tag',
          backgroundColor: Color(0xFF0A3068),

          elevation: 1.0,
          shape: CircleBorder(),
          children: [
            SpeedDialChild(
                child: Icon(
                  Icons.home,
                  color: Color(0xFF0A3068),
                  size: 17,
                ),
                backgroundColor: Colors.white,
                labelStyle: TextStyle(fontSize: 13.0),
                onTap: () {
                  setState(() {
                    _stop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyHomePage(0)));
                  });
                }),
          ],
        ),
      ),
    );
  }

  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer(mode: mode);

    _durationSubscription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);

      // TODO implemented for iOS, waiting for android impl
      if (Theme.of(context).platform == TargetPlatform.iOS) {
        // (Optional) listen for notification updates in the background
        _audioPlayer.startHeadlessService();

        // set at least title to see the notification bar on ios.
        _audioPlayer.setNotification(
            title: 'App Name',
            artist: 'Artist or blank',
            albumTitle: 'Name or blank',
            imageUrl: 'url or blank',
            forwardSkipInterval: const Duration(seconds: 30), // default is 30s
            backwardSkipInterval: const Duration(seconds: 30), // default is 30s
            duration: duration,
            elapsedTime: Duration(seconds: 0));
      }
    });

    _positionSubscription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));

    _playerCompleteSubscription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      _onComplete();
      setState(() {
        _position = _duration;
      });
    });

    _playerErrorSubscription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() => _audioPlayerState = state);
    });

    _playingRouteState = PlayingRouteState.speakers;
  }

  Future<int> _play() async {
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(
        'http://api.mp3.zing.vn/api/streaming/audio/' + baiHat.id + '/128',
        position: playPosition);
    if (result == 1)
      setState(() {
        _playerState = PlayerState.playing;
      });

    // default playback rate is 1.0
    // this should be called after _audioPlayer.play() or _audioPlayer.resume()
    // this can also be called everytime the user wants to change playback rate in the UI
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);

    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => _playerState = PlayerState.paused);
    return result;
  }

  Future<int> _earpieceOrSpeakersToggle() async {
    final result = await _audioPlayer.earpieceOrSpeakersToggle();
    if (result == 1)
      setState(() => _playingRouteState =
          _playingRouteState == PlayingRouteState.speakers
              ? PlayingRouteState.earpiece
              : PlayingRouteState.speakers);
    return result;
  }

  Future<int> _stop() async {
    final result = await _audioPlayer.stop();
    if (result == 1) {
      setState(() {
        _playerState = PlayerState.stopped;
        _position = Duration();
      });
    }
    return result;
  }

  void _onComplete() {
    setState(() => _playerState = PlayerState.stopped);
  }

  void RepeatMusic() {
    setState(() {
      if (_positionText == _durationText) Navigator.pop(context);
    });
  }
}
