import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';

import 'SignInUp.dart';

class InfomationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      home: InfomationPages("d", "d", "d", "D"),
    );
  }
}

class InfomationPages extends StatefulWidget {
  String username;
  String pass;
  String name;
  String url;
  InfomationPages(this.username, this.pass, this.name, this.url);
  @override
  _InfomationPagesState createState() => _InfomationPagesState();
}

class _InfomationPagesState extends State<InfomationPages> {
  File _image;
  @override
  void initState() {
    super.initState();
  }

  void open_camera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = image;
    });
  }

  void open_gallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF0A3068),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          height: MediaQuery.of(context).size.height - 50,
          child: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).size.height / 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 10,
                  ),
                  Text(
                    "THÔNG TIN CÁ NHÂN",
                    style: TextStyle(
                        letterSpacing: 2,
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Color(0xffE5F1FD)),
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.input,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SignInUpPage(false),
                            ));
                      })
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 30),
//ảnh đại diện

              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height / 3.5,
                  width: MediaQuery.of(context).size.height / 3.5,
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey.shade100,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.075),
                        offset: Offset(-10, -10),
                        blurRadius: 10,
                      ),
                      BoxShadow(
                        color: Colors.white,
                        offset: Offset(10, 10),
                        blurRadius: 10,
                      ),
                    ],

                    //'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true'
                  ),
                  child: Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey.shade100,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.075),
                          offset: Offset(-10, -10),
                          blurRadius: 10,
                        ),
                        BoxShadow(
                          color: Colors.white,
                          offset: Offset(-10, 10),
                          blurRadius: 10,
                        ),
                      ],

                      //'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true'
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: _image == null
                                ? NetworkImage(
                                    'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true')
                                : FileImage(_image)),
                      ),
                    ),
                  ),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height / 40),
// tên tài khoản và yteen người dung
              Center(
                child: Container(
                  child: Flexible(
                    child: Text(
                      widget.name,
                      style: TextStyle(
                          letterSpacing: 1.1,
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Color(0xffE5F1FD)),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                  child: Flexible(
                    child: Text(
                      widget.username,
                      style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 20),
// đổi tên
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: MediaQuery.of(context).size.height / 13,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xff97A4B7),
                            offset: new Offset(8.0, 10.0),
                            blurRadius: 25.0)
                      ],
                      color: Color(0xffE5F1FD),
                      borderRadius: BorderRadius.circular(15)),
                  child: Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Đổi tên",
                          style: TextStyle(
                              letterSpacing: 1,
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF0A3068)),
                        ),
                        IconButton(
                            icon:
                                Icon(Icons.mode_edit, color: Color(0xFF0A3068)),
                            onPressed: () {
                              MiniPageEditName(context);
                            })
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 20),
//đổi mật khẩu
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: MediaQuery.of(context).size.height / 13,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xff97A4B7),
                            offset: new Offset(8.0, 10.0),
                            blurRadius: 25.0)
                      ],
                      color: Color(0xffE5F1FD),
                      borderRadius: BorderRadius.circular(15)),
                  child: Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Đổi mật khẩu",
                          style: TextStyle(
                              letterSpacing: 1,
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF0A3068)),
                        ),
                        IconButton(
                            icon: Icon(Icons.vpn_key, color: Color(0xFF0A3068)),
                            onPressed: () {
                              MiniPageEditPass(context);
                            })
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 20),
//đổi hình đại diện
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: MediaQuery.of(context).size.height / 13,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xff97A4B7),
                            offset: new Offset(8.0, 10.0),
                            blurRadius: 25.0)
                      ],
                      color: Color(0xffE5F1FD),
                      borderRadius: BorderRadius.circular(15)),
                  child: Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Đổi hình đại diện",
                          style: TextStyle(
                              letterSpacing: 1,
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFF0A3068)),
                        ),
                        IconButton(
                            icon: Icon(Icons.image, color: Color(0xFF0A3068)),
                            onPressed: () {
                              MiniPageEditFace(context);
                            })
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget MiniPageEditFace(BuildContext context) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext more) {
          return Container(
            margin: const EdgeInsets.only(
                top: 10.0, right: 10, left: 10, bottom: 200),
            height: MediaQuery.of(context).size.height * 2 / 5,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Color(0xff97A4B7),
                      offset: new Offset(8.0, 10.0),
                      blurRadius: 25.0)
                ],
                color: Color(0xffE5F1FD),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Text(
                  "ẢNH ĐẠI DIỆN",
                  style: TextStyle(
                      letterSpacing: 2,
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF0A3068)),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      children: [
                        Container(
                          height: 40.0,
                          width: 40,
                          decoration: BoxDecoration(
                              color: Color(0xffE5F1FD),
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff97A4B7),
                                    offset: new Offset(8.0, 10.0),
                                    blurRadius: 25.0)
                              ]),
                          child: IconButton(
                              icon: Icon(Icons.file_upload,
                                  color: Color(0xFF0A3068)),
                              onPressed: () {
                                open_gallery();
                                Navigator.pop(context);
                              }),
                        ),
                        Text(
                          "Up file",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey),
                        )
                      ],
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width / 6,
                    ),
                    Column(
                      children: [
                        Container(
                          height: 40.0,
                          width: 40.0,
                          decoration: BoxDecoration(
                              color: Color(0xffE5F1FD),
                              shape: BoxShape.circle,
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff97A4B7),
                                    offset: new Offset(8.0, 10.0),
                                    blurRadius: 25.0)
                              ]),
                          child: IconButton(
                              icon: Icon(Icons.camera_alt,
                                  color: Color(0xFF0A3068)),
                              onPressed: () {
                                open_camera();
                                Navigator.pop(context);
                              }),
                        ),
                        Text(
                          "Camera",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          );
        });
  }

  Widget MiniPageEditName(BuildContext context) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext more) {
          return Container(
            margin: const EdgeInsets.only(
                top: 10.0, right: 10, left: 10, bottom: 200),
            height: MediaQuery.of(context).size.height * 2 / 5,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Color(0xff97A4B7),
                      offset: new Offset(8.0, 10.0),
                      blurRadius: 25.0)
                ],
                color: Color(0xffE5F1FD),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Tên bạn muốn đổi",
                  style: TextStyle(
                      letterSpacing: 2,
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF0A3068)),
                ),
                SizedBox(
                  height: 10,
                ),
                SocialBox("Tên bạn muốn đổi"),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 17,
                  width: MediaQuery.of(context).size.width / 4,
                  decoration: BoxDecoration(
                      color: Color(0xFF0A3068),
                      borderRadius: BorderRadius.circular(15)),
                  child: FlatButton(
                      onPressed: () {
                        setState(() {
                          Navigator.pop(context);
                        });
                      },
                      child: Text(
                        "Lưu",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                      )),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          );
        });
  }

  Widget MiniPageEditPass(BuildContext context) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext more) {
          return Container(
            margin: const EdgeInsets.only(
                top: 10.0, right: 10, left: 10, bottom: 10),
            height: MediaQuery.of(context).size.height * 2 / 5,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: Color(0xff97A4B7),
                      offset: new Offset(8.0, 10.0),
                      blurRadius: 25.0)
                ],
                color: Color(0xffE5F1FD),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Đổi mật khẩu mới",
                  style: TextStyle(
                      letterSpacing: 2,
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF0A3068)),
                ),
                SizedBox(
                  height: 10,
                ),
                SocialBox("Mật khẩu cũ"),
                SizedBox(
                  height: 10,
                ),
                SocialBox("Mật khẩu mới"),
                SizedBox(
                  height: 10,
                ),
                SocialBox("Nhập lại mật khẩu mới"),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 17,
                  width: MediaQuery.of(context).size.width / 4,
                  decoration: BoxDecoration(
                      color: Color(0xFF0A3068),
                      borderRadius: BorderRadius.circular(15)),
                  child: FlatButton(
                      onPressed: () {
                        setState(() {
                          Navigator.pop(context);
                        });
                      },
                      child: Text(
                        "Lưu",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                      )),
                ),
              ],
            ),
          );
        });
  }
}

class SocialBox extends StatelessWidget {
  final String category;

  const SocialBox(
    this.category,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20),
      width: MediaQuery.of(context).size.width * 2 / 3,
      height: MediaQuery.of(context).size.height / 15,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xffE5F1FD).withOpacity(.8),
          boxShadow: [
            BoxShadow(
                color: Color(0xff97A4B7),
                offset: new Offset(8.0, 10.0),
                blurRadius: 25.0)
          ]),
      child: TextField(
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: category,
              hintStyle: TextStyle(
                  letterSpacing: 2,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey))),
    );
  }
}
