import 'dart:core';

import 'package:flutter/material.dart';

class info {
  String username;
  String password;
  String name;
  String urlimage;
  info(String usename, String pass, String name, String urlimage) {
    this.username = usename;
    this.password = pass;
    this.name = name;
    this.urlimage = urlimage;
  }
  set Username(String username) => this.username = username;
  set Pass(String pass) => this.password = pass;
  set Name(String name) => this.name = name;
  set urlImage(String url) => this.urlimage = url;

  get Username {
    return this.username;
  }

  get Pass {
    return this.password;
  }

  get Name {
    return this.name;
  }

  get urlImage {
    return this.urlimage;
  }
}
