import 'package:flutter/material.dart';
import 'package:loadplaylist/PlayList/Baihat.dart';

class LibraryPlayList {
  int id;
  String NamePlayList;
  String UrlImage;
  int SoSong;
  String UrlPlayList;
  List<BaiHat> SongInPlayList;
  LibraryPlayList(
      int id, String Name, String image, int sosong, String urlplaylist) {
    this.id = id;
    this.NamePlayList = Name;
    this.UrlImage = image;
    this.SoSong = sosong;
    this.UrlPlayList = urlplaylist;
  }
  set SetID(int id) => this.id = id;
  set SetNamePL(String name) => this.NamePlayList = name;
  set SetURLImage(String url) => this.UrlImage = url;
  set SetSoSong(int sosong) => this.SoSong = sosong;
  set SetURLPlauList(String url) => this.UrlPlayList = url;
  get GetID {
    return this.id;
  }

  get GetName {
    return this.NamePlayList;
  }

  get GetURLImage {
    return this.UrlImage;
  }

  get GetSoSong {
    return this.SoSong;
  }

  get GetURLPlayList {
    return this.UrlPlayList;
  }
}
