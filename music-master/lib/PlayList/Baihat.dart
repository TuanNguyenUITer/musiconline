class BaiHat {
  var id;
  String ten;
  String casi;
  String hinhanh;
  String hinhanhlon;
  var key;
  String linkGoc;
  String linkLyric;
  String linkStream;
  var kbit;
  var duration;
  BaiHat(String id, String ten, String casi, String hinhanh, String hinhanhlon,
    String linkLyric, String duration) {
    this.id = id;
    this.ten = ten;
    this.casi = casi;
    this.hinhanh = hinhanh;
    this.hinhanhlon = hinhanhlon;
    this.linkLyric = linkLyric;
    this.duration = int.parse(duration);
    linkStream = 'http://api.mp3.zing.vn/api/streaming/audio/'
      + id.toString() + '/128';
  }
  String get title => this.ten;
  BaiHat.fromJson(Map<String, dynamic> json)
  {
    this.id = json['id'];
    this.ten = json['title'];
    this.casi = json['artists_names'];
    this.hinhanh = json['thumbnail'];
    this.hinhanhlon = json['thumbnail_medium'];
    this.linkLyric = json['lyric'];
    this.duration = json['duration'];
  }
}