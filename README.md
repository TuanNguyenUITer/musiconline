# MusicOnline

#**Ứng dụng nghe nhạc trực tuyến MusicOnline**

#**Đồ án môn Lập trình trên thiết bị di động - SE346.K21**

**##Sinh viên:**

**##Nguyễn Văn Tuấn -17521218**

**##Lê Hoàng Long - 17520709**

**#Giới thiệu**

Là ứng dụng nghe nhạc trực tuyến , sử dụng api miễn phí từ [zingmp3.vn](https://zingmp3.vn/)

Phát triển dựa trên framwork [Flutter](https://flutter.dev/docs) 

Các gói thư viện dùng chính: 

[http](https://pub.dev/packages/http): ^0.12.1. [audioplayers](https://pub.dev/packages/audioplayers): ^0.15.1 và một số thư viện hỗ trợ khác

#Hướng phát triển

#**Chức năng**

#-Ứng dụng cung cấp các danh sách nhạc đa dạng và sinh động , phù hợp với mọi lứa tuổi như:

+ Các bài hát hot Top 100 nhạc trữ tình , cách mạng, nhạc trẻ , nhạc IDM
+ Cách danh sách nhạc gợi ý , nhạc việt , nhạc bolero, nhạc Rap, nhạc ...
+ Và các thể loại nhạc nước ngoài US-UK, K-Pop. C-Pop
+ Đăng nhập tài khoản facebook, google để lưu cá nhân hóa sở thích nghe nhạc của mình, thả yêu thích ,bình luận cảm xúc (Đang trong quá trình phát triển).
+ Tải nhạc về máy để nghe nhạc offline (Đang trong quá trình phát triển)

#**#Hướng phát triển:**
+ Nhóm em dự định  sẽ khắc phục một số lỗi logic và hoàn thiện các chức năng chưa hoàn thành như tải nhạc về và bình luận , thả tim ..., đồng thời cập nhật nội dung
để ứng dụng đa dạng hơn ... 

#**Review màn hình **

#Màn hình đăng nhập

![dangnhap](/uploads/f7b07bc5fcb44a184eac3aa277d1c532/dangnhap.png)


#Màn hình Trang chủ 

![home](/uploads/d6454314e4909cd5cf81056ef590f439/home.png)

#Màn hình tran chủ

![home2](/uploads/25430fa8f8565e4eb3afa1e6b9a9ea4b/home2.png)

#Thư viện 

![Thuvien](/uploads/33e90f159d8f0683b85c0697419fd526/Thuvien.png)

#Màn hình tìm kiếm nhạc

![timkiem](/uploads/89362b211b56d5c98010b48a65c9e248/timkiem.png)


![cothenghe](/uploads/ca537e3588451602cbbe0755de4b3700/cothenghe.png)

#Đăng ký tài khoản

![dangky](/uploads/5463ca2efa07db0185e5be58f9b0cdb7/dangky.png)ư

#Màn hình phát nhạc

![Dangphat](/uploads/4e9ac6c3604d5fcb702363eba8250480/Dangphat.png)